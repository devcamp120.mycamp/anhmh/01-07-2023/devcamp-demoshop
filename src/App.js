import Header from './components/Header';
import Container from './components/Container';
import Footer from './components/Footer';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../src/App.css';


function App() {
  return (
    <div>
     <Header></Header>
     <Container></Container>
     <Footer></Footer>
    </div>
  );
}

export default App;
