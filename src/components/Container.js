import React from 'react';

function Container() {
    return (
        <div className="container main-container">
            <div className="container">
                <h3>Product List</h3>
                <p>Showing 1 - 9 of 24 products</p>
            </div>
            <div className="row">
                <div className="col-6 col-sm-4">
                    <div className="product-box card bg-light mb-3">
                        <div className="card-header">
                            <h5 className="card-title">
                                <a href="/product-detail/1">Banana</a>
                            </h5>
                        </div>
                        <div className="card-body">
                            <div className="text-center">
                                <a href="/product-detail/1">
                                    <img className="card-img-top" alt="Banana" src="https://www.loop54.com/hubfs/demo_images/banana.jpg" />
                                </a>
                            </div>
                            <p className="card-text description">The banana is an edible fruit, botanically a berry, produced by several kinds of large herbaceous...</p>
                            <p className="card-text"><b>Category:</b> Fruit</p>
                            <p className="card-text"><b>Made by:</b> The banana company</p>
                            <p className="card-text"><b>Organic:</b> Yes</p>
                            <p className="card-text"><b>Price:</b> $7</p>
                            <div className="add-card-container">
                                <button type="button" className="btn btn-primary">Add to cart</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-6 col-sm-4">
                    <div className="product-box card bg-light mb-3">
                        <div className="card-header">
                            <h5 className="card-title">
                                <a href="/product-detail/2">Cinnamon roll</a>
                            </h5>
                        </div>
                        <div className="card-body">
                            <div className="text-center">
                                <a href="/product-detail/2">
                                    <img className="card-img-top" alt="Cinamon" src="https://www.loop54.com/hubfs/demo_images/cinnamonroll.jpg" />
                                </a>
                            </div>
                            <p className="card-text description">A cinnamon roll (also cinnamon bun, cinnamon swirl, cinnamon Danish and cinnamon snail) is a swee...</p>
                            <p className="card-text"><b>Category:</b> Bakery</p>
                            <p className="card-text"><b>Made by:</b> Awesome bakery</p>
                            <p className="card-text"><b>Organic:</b> No</p>
                            <p className="card-text"><b>Price:</b> $5</p>
                            <div className="add-card-container">
                                <button type="button" className="btn btn-primary">Add to cart</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-6 col-sm-4">
                    <div className="product-box card bg-light mb-3">
                        <div className="card-header">
                            <h5 className="card-title">
                                <a href="/product-detail/3">Apple</a>
                            </h5>
                        </div>
                        <div className="card-body">
                            <div className="text-center">
                                <a href="/product-detail/3">
                                    <img className="card-img-top" alt="Apple" src="https://www.loop54.com/hubfs/demo_images/apple.jpg" />
                                </a>
                            </div>
                            <p className="card-text description">The apple tree (Malus domestica) is a deciduous tree in the rose family best known for its sweet,....</p>
                            <p className="card-text"><b>Category:</b> Fruit</p>
                            <p className="card-text"><b>Made by:</b> Fruits n Veggies</p>
                            <p className="card-text"><b>Organic:</b> No</p>
                            <p className="card-text"><b>Price:</b> $6</p>
                            <div className="add-card-container">
                                <button type="button" className="btn btn-primary">Add to cart</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-6 col-sm-4">
                    <div className="product-box card bg-light mb-3">
                        <div className="card-header">
                            <h5 className="card-title">
                                <a href="/product-detail/4">Minced meat</a>
                            </h5>
                        </div>
                        <div className="card-body">
                            <div className="text-center">
                                <a href="/product-detail/4">
                                    <img className="card-img-top" alt="Minced-meat" src="https://www.loop54.com/hubfs/demo_images/mincedmeat.jpg" />
                                </a>
                            </div>
                            <p className="card-text description">Ground beef, beef mince, minced meat, hamburger (in the United States) is a ground meat made of b...</p>
                            <p className="card-text"><b>Category:</b> Meat</p>
                            <p className="card-text"><b>Made by:</b> UltraBovine</p>
                            <p className="card-text"><b>Organic:</b> No</p>
                            <p className="card-text"><b>Price:</b> $60</p>
                            <div className="add-card-container">
                                <button type="button" className="btn btn-primary">Add to cart</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-6 col-sm-4">
                    <div className="product-box card bg-light mb-3">
                        <div className="card-header">
                            <h5 className="card-title">
                                <a href="/product-detail/5">Milk</a>
                            </h5>
                        </div>
                        <div className="card-body">
                            <div className="text-center">
                                <a href="/product-detail/5">
                                    <img className="card-img-top" alt="Milk" src="https://www.loop54.com/hubfs/demo_images/milk.jpg" />
                                </a>
                            </div>
                            <p className="card-text description">Milk is a white liquid produced by the mammary glands of mammals. It is the primary source of nut....</p>
                            <p className="card-text"><b>Category:</b> Dairy</p>
                            <p className="card-text"><b>Made by:</b> Early</p>
                            <p className="card-text"><b>Organic:</b> No</p>
                            <p className="card-text"><b>Price:</b> $10</p>
                            <div className="add-card-container">
                                <button type="button" className="btn btn-primary">Add to cart</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-6 col-sm-4">
                    <div className="product-box card bg-light mb-3">
                        <div className="card-header">
                            <h5 className="card-title">
                                <a href="/product-detail/6">Organic Milk</a>
                            </h5>
                        </div>
                        <div className="card-body">
                            <div className="text-center">
                                <a href="/product-detail/6">
                                    <img className="card-img-top" alt="Organic Milk" src="https://www.loop54.com/hubfs/demo_images/organicmilk.jpg" />
                                </a>
                            </div>
                            <p className="card-text description">Milk is a white liquid produced by the mammary glands of mammals. It is the primary source of nut....</p>
                            <p className="card-text"><b>Category:</b> Dairy</p>
                            <p className="card-text"><b>Made by:</b> Early</p>
                            <p className="card-text"><b>Organic:</b> Yes</p>
                            <p className="card-text"><b>Price:</b> $12</p>
                            <div className="add-card-container">
                                <button type="button" className="btn btn-primary">Add to cart</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-6 col-sm-4">
                    <div className="product-box card bg-light mb-3">
                        <div className="card-header">
                            <h5 className="card-title">
                                <a href="/product-detail/7">Organic Apple</a>
                            </h5>
                        </div>
                        <div className="card-body">
                            <div className="text-center">
                                <a href="/product-detail/7">
                                    <img className="card-img-top" alt="Banana" src="https://www.loop54.com/hubfs/demo_images/greenapple.jpg" />
                                </a>
                            </div>
                            <p className="card-text description">The apple tree (Malus domestica) is a deciduous tree in the rose family best known for its sweet,...</p>
                            <p className="card-text"><b>Category:</b> Fruit</p>
                            <p className="card-text"><b>Made by:</b> The banana company</p>
                            <p className="card-text"><b>Organic:</b> Yes</p>
                            <p className="card-text"><b>Price:</b> $7</p>
                            <div className="add-card-container">
                                <button type="button" className="btn btn-primary">Add to cart</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-6 col-sm-4">
                    <div className="product-box card bg-light mb-3">
                        <div className="card-header">
                            <h5 className="card-title">
                                <a href="/product-detail/8">Ground beef</a>
                            </h5>
                        </div>
                        <div className="card-body">
                            <div className="text-center">
                                <a href="/product-detail/8">
                                    <img className="card-img-top" alt="Ground beef" src="https://www.loop54.com/hubfs/demo_images/groundbeef.jpg" />
                                </a>
                            </div>
                            <p className="card-text description">Ground beef, beef mince, minced meat, hamburger (in the United States) is a ground meat made of b...</p>
                            <p className="card-text"><b>Category:</b> Meat</p>
                            <p className="card-text"><b>Made by:</b> MeatNStuff</p>
                            <p className="card-text"><b>Organic:</b> Yes</p>
                            <p className="card-text"><b>Price:</b> $65</p>
                            <div className="add-card-container">
                                <button type="button" className="btn btn-primary">Add to cart</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-6 col-sm-4">
                    <div className="product-box card bg-light mb-3">
                        <div className="card-header">
                            <h5 className="card-title">
                                <a href="/product-detail/9">Steak</a>
                            </h5>
                        </div>
                        <div className="card-body">
                            <div className="text-center">
                                <a href="/product-detail/9">
                                    <img className="card-img-top" alt="Banana" src="http://www.loop54.com/hubfs/demo_images/steak.jpg" />
                                </a>
                            </div>
                            <p className="card-text description">A steak is a meat generally sliced perpendicular to the muscle fibers, potentially including a bo...</p>
                            <p className="card-text"><b>Category:</b> Meat</p>
                            <p className="card-text"><b>Made by:</b> UltraBovine</p>
                            <p className="card-text"><b>Organic:</b> Yes</p>
                            <p className="card-text"><b>Price:</b> $100</p>
                            <div className="add-card-container">
                                <button type="button" className="btn btn-primary">Add to cart</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Container
