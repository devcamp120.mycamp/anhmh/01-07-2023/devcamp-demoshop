import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'

function Footer() {
    return (
        <div className="bg-dark footer-container">
            <div className="container">
                <div className="row row-30 footer-row">
                    <div className="col-md-4 col-xl-5">
                        <div className="pr-xl-4">
                            <p className="text-light">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <p className="text-light copyright-year">©  2018 . All Rights Reserved.</p>
                        </div>

                    </div>
                    <div className="col-md-4">
                        <h5 className="text-light">Contacts</h5>
                        <dl className="contact-list">
                            <dt className="text-light">Address:</dt>
                            <dd className="text-light">Kolkata, West Bengal, India</dd>
                        </dl>
                        <dl className="contact-list">
                            <dt className="text-light">email:</dt>
                            <dd className="text-light"><a href="mailto#">info@example.com</a></dd>
                        </dl>
                        <dl className="contact-list">
                            <dt className="text-light">phones:</dt>
                            <dd className="text-light"><a href="tel:#">+91 99999999</a> or <a href="tel:#">+91 11111111</a></dd>
                        </dl>
                    </div>
                    <div className="col-md-4 col-xl-3">
                        <h5 className="text-light">Links</h5>
                        <ul className="nav-list text-light">
                            <li><a href="about:#"></a>About</li>
                            <li>Projects</li>
                            <li>Blog</li>
                            <li>Contacts</li>
                            <li>Pricing</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div className="row no-gutters social-container">
                <div className="col">
                    <a className="social-inner text-light align-center" href="#facebook">
                      FACEBOOK
                    </a>
                </div>
                <div className="col">
                    <a className="social-inner" href="#instagram">
                        <span className="icon mdi mdi-instagram">
                            <span>Instagram</span>
                        </span>
                    </a>
                </div>
                <div className="col">
                    <a className="social-inner" href="#twitter">
                        <span className="icon mdi mdi-twitter">
                            <span>Twitter</span>
                        </span>
                    </a>
                </div>
                <div className="col">
                    <a className="social-inner" href="#google">
                        <span className="icon mdi mdi-google">
                            <span>Google</span>
                        </span>
                    </a>
                </div>
            </div>
        </div>

    )
}

export default Footer
