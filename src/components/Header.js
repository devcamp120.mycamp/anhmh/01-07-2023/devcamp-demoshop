import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import image from '../../src/assets/React-icon.png';

function Header() {
    return (
        <div>
            <div className="logo">
                <a className="navbar-brand" href="#">
                    <img src={image}  width="48" height="42" alt="Logo"/>
                </a>
            </div>
            <div className="container text-center">
                <h1>React Store</h1>
                <p>Demo App Shop24h v1.0</p>
            </div>
            <nav className="navbar bg-dark border-bottom border-bottom-dark" data-bs-theme="dark">
                <div className="container-fluid">
                    <div className="navbar-nav">
                        <a className="nav-link active" aria-current="page" href="#">Home</a>
                    </div>
                </div>
            </nav>
        </div>
    )
}

export default Header
